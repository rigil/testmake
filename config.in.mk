
PREFIX = /usr/local

# X
X11FLAGS	= `pkg-config --cflags x11`
X11LIBS		= `pkg-config --libs x11`

# flags
CC_FLAGS = -Wall -Wextra -pedantic -std=c99 -D_DEFAULT_SOURCE $(X11FLAGS)
LD_FLAGS = $(X11LIBS)

# tools
CC	= cc
LD	= $(CC)

# vim:set ts=2 sw=2 noet:
