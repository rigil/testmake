#ifndef UTIL_H
#define UTIL_H

#define UNUSED(x)   ((void)(x))
#define SAFESTR(s)  ((s) ? (s) : "")

void die(const char *fmt, ...);
const char *fname(const char *path);

#endif /* UTIL_H */
/* vim:set ts=2 sw=2 et: */
