#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

void
die(const char *fmt, ...) {
  int error = errno;
  va_list va;
  va_start(va, fmt);
  vfprintf(stderr, fmt, va);
  va_end(va);

  if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
    fprintf(stderr, " [%d] ", error);
    errno = error;
    perror(NULL);
  }
  else if (fmt[0] && fmt[strlen(fmt)-1] != '\n') {
    fputc('\n', stderr);
  }
  exit(EXIT_FAILURE);
}

const char *
fname(const char *path) {
  const char *f = strrchr(path, '/');
  if (!f) f = path;
  else ++f;
  return f;
}

/* vim:set ts=2 sw=2 et: */
