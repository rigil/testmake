#include <errno.h>
#include <locale.h>
#include <signal.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include "util.h"

static const char *argv0;
static Display *dpy = 0;

static void cleanup(void);
static void sighandler(int sig);
static void usage(void);

int
main(int argc, char *argv[]) {
  /* setup */
  {
    const char *opt_dpy = NULL;
    unsigned char opt_sync = 0;

    argv0 = *argv;
#define CMDOPT(opt, bstatment)  if (opt != 0 && argv[0][1] == opt) { bstatment; continue; }
#define CMDARG(opt, svar)       if (opt != 0 && argv[0][1] == opt && (argc > 1)) { svar = *++argv; --argc; continue; }
    for (--argc, ++argv; argv[0] && argv[0][0] == '-' && argv[0][1]; --argc, ++argv) {
      if (argv[0][1] == '-' && argv[0][2] == '\0') { --argc; ++argv; break; }
      CMDARG('d', opt_dpy);
      CMDOPT('x', opt_sync = 1);
      CMDOPT('h', usage());
    }
#undef CMDOPT
#undef CMDARG

    /* system stuff */
    atexit(cleanup);
    signal(SIGINT, sighandler);
    signal(SIGTERM, sighandler);
    signal(SIGCHLD, sighandler);

    /* setup locale */
    setlocale(LC_ALL, "");
    if (!XSupportsLocale()) setlocale(LC_ALL, "C");
    XSetLocaleModifiers("");
    setlocale(LC_NUMERIC, "C");

    /* setup display */
    dpy = XOpenDisplay(opt_dpy);
    if (!dpy) die("cannot connect to display '%s'", SAFESTR(opt_dpy));
    if (opt_sync) XSynchronize(dpy, True);
  }
  /* run loop */
  {
    XEvent ev;
    fd_set fds;
    int maxfd = -1;
    int selected = -1;
    int dpyfd = ConnectionNumber(dpy);
    int running = 1;
    while (running) {
      maxfd = dpyfd;
      FD_ZERO(&fds);
      FD_SET(dpyfd, &fds);

      do { selected = select(maxfd + 1, &fds, NULL, NULL, NULL);
      } while (selected == -1 && errno == EINTR);
      if (selected < 0) {
        if (errno == EBADF) running = 0; /* normal if we break X */
        else die("select:");
      }
      if (selected > 0) {
        if (FD_ISSET(dpyfd, &fds)) {
          /* handle X events */
          while (!XNextEvent(dpy, &ev)) {
            switch (ev.type) {
            default: break;
            }
          }
        }
      }
    }
  }
  /* finish up */
  {
    cleanup();
  }
  return 0;
}

static
void cleanup(void) {
  if (dpy) {
    XSetInputFocus(dpy, PointerRoot, 0, CurrentTime);
    XCloseDisplay(dpy);
    dpy = 0;
  }
}

static
void sighandler(int sig) {
  switch (sig) {
  case SIGINT:
  case SIGTERM:
    cleanup();
    break;
  case SIGCHLD:
    break;
  }
}

static
void usage(void) {
  die("usage: %s [options]\n"
      "    -d display connect to specified display\n"
      "    -h         show this help\n"
      "    -x         run X in synchronized mode (for debugging)\n"
      , fname(argv0));
}

/* vim:set ts=2 sw=2 et: */
