.POSIX:

include config.mk

NAM = testmake
SRC = main.c util.c
OBJ = $(SRC:.c=.o)
ADD = util.h config.mk

all: options $(NAM)

$(OBJ): $(ADD)

clean:
	@rm -f $(NAM) $(OBJ)

config.mk: config.in.mk
	@cp -f $< $@

$(NAM): $(OBJ)
	$(LD) -o $@ $(OBJ) $(LD_FLAGS) $(LDFLAGS)

.c.o:
	$(CC) -o $@ -c $(CC_FLAGS) $(CFLAGS) $<

options:
	@echo $(NAME) build options:
	@echo "    C FLAGS   $(CC_FLAGS) $(CFLAGS)"
	@echo "    LD FLAGS  $(LD_FLAGS) $(LDFLAGS)"
	@echo "    CC        $(CC)"
	@echo "    LD        $(LD)"

.PHONY: all clean options
.SUFFIXES:
.SUFFIXES: .c .h .o

# vim:set ts=2 sw=2 noet:
